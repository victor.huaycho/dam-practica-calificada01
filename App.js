import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Home from './app/components/Home';
import vistaDetalles from './app/components/vistaDetalles';

const Stack = createStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Lista" component={Home}/>
        <Stack.Screen name="Detalles" component={vistaDetalles} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App