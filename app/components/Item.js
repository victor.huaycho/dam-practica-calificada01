import * as React from 'react';
import { View, Text, Button, Image, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    contenedor: {
        width: '100%',
        flexDirection: 'row',
        marginTop: 3,
        marginBottom: 3,
        padding: 10,
        borderRadius:5,
        backgroundColor:'white',
        shadowColor:'black',
        shadowOpacity: 0.58,
        shadowRadius: 16.00,
        elevation: 10
    },
    texto: {
        width:270,
        marginLeft:10,
    },
    imagen: {
        height:100,
        width: 100,
        borderTopLeftRadius:  30,
        borderBottomRightRadius:  30
    },
    contenedorImagen: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    contenedorButton: {
        width: '15%',
        justifyContent: 'center',
        alignItems: 'center',
    },
})

function Item(props){
    return(
        <View style={styles.contenedor}>
            <View style={styles.contenedorImagen}>
                <Image style={styles.imagen} source={{uri: props.image}}/>
            </View>

            <View style={styles.texto}>
                <Text style={{ flex: 1, fontSize:25 }}>{props.titulo}</Text>
                <Text numberOfLines={4} style={{ textAlign: 'left' }}>{props.resumen}</Text>
            </View>
            <View style={styles.contenedorButton}>
                <Button
                    title="->"
                    onPress={ () => props.navigation.navigate('Detalles', {
                        titulo: props.titulo,
                        imagen: props.image,
                        texto: props.resumen
                    })}
                    color="#ff5c5c"
                />
            </View>
        </View>
    )
}

export default Item