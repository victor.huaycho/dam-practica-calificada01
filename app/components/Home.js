import React, { useState, useEffect } from 'react';
import { View, FlatList } from 'react-native';
import Item from './Item';

function Home({ navigation }) {
    const [lista, setLista] = useState([])

    useEffect(() => {
        fetch(
          "https://yts.mx/api/v2/list_movies.json"
        )
        .then(res => res.json())
        .then(
            result => {
                setLista(result.data.movies)
            },
        )}
    )
    return (
        <View style={{padding:5}}>
            <FlatList
                data={lista.length > 0 ? lista : []} renderItem={({item})=>{
                    return(
                    <Item image={item.medium_cover_image} titulo={item.title} resumen={item.summary} navigation={navigation}/>)
                }}
                keyExtractor = {item => item.id}
            />
        </View>
    );
}

export default Home

