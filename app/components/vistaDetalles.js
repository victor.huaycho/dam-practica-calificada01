import * as React from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    contenedor: {
        flex: 1, 
        alignItems: 'center', 
        justifyContent: 'center',
        margin:20,
        marginTop: 3,
        marginBottom: 3,
        padding: 10,
        borderRadius:5,
        backgroundColor:'white',
        shadowColor:'black',
        shadowOpacity: 0.58,
        shadowRadius: 16.00,
        elevation: 10
    },
    imagen: {
        width: '80%', 
        height: '50%', 
        marginBottom: 15
    }
})

function vistaDetalles({ route }) {
    const { titulo, imagen, texto } = route.params

    return (
      <View style={styles.contenedor}>
        <Text style={{ fontSize:30, marginBottom: 15 }}>{titulo}</Text>
        <Image style={styles.imagen} source={{uri: imagen}}></Image>
        <Text style={{ fontSize: 20, width: 380 }}>{texto}</Text>
      </View>
    );
  }

  export default vistaDetalles